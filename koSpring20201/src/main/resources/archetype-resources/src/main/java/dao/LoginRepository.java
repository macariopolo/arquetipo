package ${package}.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ${package}.model.Login;

public interface LoginRepository extends JpaRepository <Login, String> {

}
