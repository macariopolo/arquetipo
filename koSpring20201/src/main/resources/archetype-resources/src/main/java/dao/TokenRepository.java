package ${package}.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ${package}.model.Token;

public interface TokenRepository extends JpaRepository<Token, String> {

}
